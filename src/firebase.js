import * as firebase from "firebase";

const app = firebase.initializeApp({
  apiKey: "AIzaSyCvBuiA7eSnkZkU7wSTgG77rgwSgR92MVY",
  authDomain: "abnormal-weather.firebaseapp.com",
  databaseURL: "https://abnormal-weather.firebaseio.com",
  projectId: "abnormal-weather",
  storageBucket: "abnormal-weather.appspot.com",
  messagingSenderId: "592344067975"
});

const database = app.database();

export default database;
