import Today from "./Today";

it("renders without errors", () => {
  const wrapper = shallow(<Today date="2018-02-05 10:10:10" />);

  expect(wrapper).toMatchSnapshot();
});
