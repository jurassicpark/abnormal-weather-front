import Daily from "./Daily";

it("renders component correctly", () => {
  const wrapper = shallow(
    <Daily date="2018-02-05 10:10:10" low={5} high={10} />
  );

  expect(wrapper).toMatchSnapshot();
});
