import Current from "./Current";
import sinon from "sinon";

it("displays current weather", () => {
  const wrapper = render(
    <Current temp={10} city="Toronto" toggleForecast={() => {}} />
  );
  expect(wrapper).toMatchSnapshot();
});

it("calls callback when clicked", () => {
  const spy = sinon.spy();
  const wrapper = mount(
    <Current temp={10} city="Toronto" toggleForecast={spy} />
  );

  wrapper
    .find("div")
    .first()
    .simulate("click");

  expect(spy.calledOnce).toBe(true);
});

it("formats temperature correctly", () => {
  const wrapper = mount(
    <Current temp={10} city="Toronto" toggleForecast={() => {}} />
  );

  const text = wrapper.find("LargeText").text();

  expect(text).toEqual("10°c");
});
