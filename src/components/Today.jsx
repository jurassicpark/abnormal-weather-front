import React from "react";
import PropTypes from "prop-types";
import moment from "moment";
import { TodayBox, LargeText, SmallText } from "../elements/box";

export default class Today extends React.Component {
  static propTypes = {
    date: PropTypes.string.isRequired
  };

  render() {
    const date = moment(this.props.date);

    return (
      <TodayBox>
        <LargeText>{date.format("Do")}</LargeText>
        <SmallText>{date.format("MMMM, YYYY")}</SmallText>
      </TodayBox>
    );
  }
}
