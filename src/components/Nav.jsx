import React from "react";
import PropTypes from "prop-types";

import { NavBar, HomeLink, HomeImage, CityName } from "../elements/nav";

import homeImage from "../images/home.png";

export default class Nav extends React.Component {
  static propTypes = {
    city: PropTypes.string.isRequired
  };

  render() {
    return (
      <NavBar>
        <HomeLink to="/">
          <HomeImage src={homeImage} alt="home trigger" />
        </HomeLink>

        <CityName>{this.props.city}</CityName>
      </NavBar>
    );
  }
}
