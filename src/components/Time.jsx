import React from "react";
import PropTypes from "prop-types";
import moment from "moment";
import { TimeBox, LargeText, SmallText } from "../elements/box";

export default class Time extends React.Component {
  static propTypes = {
    time: PropTypes.string.isRequired
  };

  render() {
    const time = moment(this.props.time);

    return (
      <TimeBox>
        <LargeText>{time.format("h:mm")}</LargeText>
        <SmallText>{time.format("a")}</SmallText>
      </TimeBox>
    );
  }
}
