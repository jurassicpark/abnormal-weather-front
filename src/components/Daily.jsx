import React from "react";
import PropTypes from "prop-types";
import moment from "moment";
import { DailyBox, Day, HighLow, Small } from "../elements/daily";

export default class Daily extends React.Component {
  static propTypes = {
    date: PropTypes.string.isRequired,
    low: PropTypes.number.isRequired,
    high: PropTypes.number.isRequired
  };

  render() {
    const date = moment(this.props.date);

    return (
      <DailyBox>
        <Day>{date.format("ddd")}</Day>
        <HighLow>
          <Small>&darr;</Small> {this.props.low} <Small>&uarr;</Small>{" "}
          {this.props.high}
        </HighLow>
      </DailyBox>
    );
  }
}
