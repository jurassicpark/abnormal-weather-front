import React from "react";
import PropTypes from "prop-types";
import { inject, observer } from "mobx-react";

import {
  ChatContainer,
  MessageList,
  MessageItem,
  UserInput,
  TextInput,
  SendButton,
  MessageName,
  MessageText,
  ChatTrigger,
  TriggerImage,
  Form
} from "../elements/chat";

import chatImage from "../images/chat.png";

import database from "../firebase";

@inject("UiStore")
@observer
export default class Chat extends React.Component {
  static propTypes = {
    city: PropTypes.string.isRequired
  };

  constructor(props) {
    super(props);

    this.state = {
      messages: {}
    };
  }

  componentDidMount() {
    const city = this.props.city.toLowerCase();
    const cityRef = database.ref(`cities/${city}`);
    cityRef.limitToLast(20).on("value", snapshot => {
      this.setState({
        messages: snapshot.val() || {}
      });
    });
  }

  onSend = e => {
    e.preventDefault();

    const city = this.props.city.toLowerCase();
    const user = this.userInput.value;
    const text = this.textInput.value;

    const messageRef = database.ref(`cities/${city}`).push();
    messageRef.set({
      user,
      text,
      time: new Date().getTime()
    });

    this.textInput.value = "";
  };

  render() {
    const { UiStore } = this.props;

    return (
      <ChatContainer chat={UiStore.showChat}>
        <ChatTrigger
          onClick={e => {
            e.preventDefault();
            UiStore.toggleChat();
          }}
        >
          <TriggerImage src={chatImage} alt="chat icon trigger" />
        </ChatTrigger>

        <MessageList>
          {Object.values(this.state.messages).map(message => (
            <MessageItem key={message.time}>
              <MessageName>{message.user}</MessageName>
              <MessageText>{message.text}</MessageText>
            </MessageItem>
          ))}
        </MessageList>

        <Form
          onSubmit={e => {
            this.onSend(e);
          }}
        >
          <UserInput
            type="text"
            innerRef={elem => (this.userInput = elem)}
            placeholder="Your name"
          />
          <TextInput
            innerRef={elem => (this.textInput = elem)}
            placeholder="Your message"
          />
          <SendButton type="submit">Send</SendButton>
        </Form>
      </ChatContainer>
    );
  }
}
