import Time from "./Time";

it("renders the Time component", () => {
  const component = render(<Time time="2017-01-15 10:10:10" />);
  expect(component).toMatchSnapshot();
});
