import React from "react";
import PropTypes from "prop-types";
import { CurrentBox, LargeText, SmallText } from "../elements/box";

const Current = ({ temp, city, toggleForecast }) => (
  <CurrentBox
    onClick={e => {
      e.preventDefault();
      toggleForecast();
    }}
  >
    <LargeText>{temp}&deg;c</LargeText>
    <SmallText>in {city}</SmallText>
  </CurrentBox>
);

Current.propTypes = {
  temp: PropTypes.number.isRequired,
  city: PropTypes.string.isRequired,
  toggleForecast: PropTypes.func.isRequired
};

export default Current;
