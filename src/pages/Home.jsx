import React from "react";
import PlacesAutocomplete from "react-places-autocomplete";
import { inject, observer } from "mobx-react";
import {
  Container,
  Form,
  Label,
  NearbyCities,
  NearbyCity,
  NearbyLink,
  Logo,
  autoComplete
} from "../elements/home";

import logo from "../images/theclima.svg";

@inject("WeatherStore")
@observer
export default class Home extends React.Component {
  componentWillMount() {
    if ("geolocation" in navigator) {
      this.props.WeatherStore.loadNearby();
    }
  }

  search = address => {
    this.props.history.push(`/${address}`);
  };

  render() {
    const { WeatherStore } = this.props;

    return (
      <Container>
        <Logo src={logo} alt="The Clima" />
        <Form>
          <Label for="city">City</Label>
          <PlacesAutocomplete
            inputProps={{
              value: WeatherStore.address,
              placeholder: "Start typing...",
              onChange: address => {
                WeatherStore.address = address;
              }
            }}
            onSelect={address => {
              this.search(address);
            }}
            styles={autoComplete}
          />
        </Form>
        <NearbyCities>
          {WeatherStore.nearby.map(city => (
            <NearbyCity key={city.name}>
              <NearbyLink to={`/${city.name}`}>{city.name}</NearbyLink>
            </NearbyCity>
          ))}
        </NearbyCities>
      </Container>
    );
  }
}
