import React from "react";
import { Motion, spring } from "react-motion";
import { inject, observer } from "mobx-react";

import Nav from "../components/Nav";
import Daily from "../components/Daily";
import Current from "../components/Current";
import Time from "../components/Time";
import Today from "../components/Today";
import Chat from "../components/Chat";

import {
  Loading,
  CityContainer,
  CityBackground,
  Forecast
} from "../elements/city";

@inject("UiStore", "WeatherStore")
@observer
export default class City extends React.Component {
  componentDidMount() {
    const { WeatherStore, match } = this.props;

    setInterval(() => {
      WeatherStore.tick();
    }, 500);

    WeatherStore.fetchWeather(match.params.city);
  }

  render() {
    const { WeatherStore, UiStore } = this.props;
    const weather = WeatherStore.weather;
    const { city } = this.props.match.params;

    if (WeatherStore.loadWeatherError) {
      return <Loading>Error loading {city}</Loading>;
    } else if (!weather) {
      return <Loading>Loading...</Loading>;
    }

    return (
      <CityContainer chat={UiStore.showChat}>
        <CityBackground
          style={{
            backgroundImage: `url('${weather.image_url}')`
          }}
        />
        <Nav city={weather.city} />

        <Motion
          defaultStyle={{ opacity: 0, x: -100 }}
          style={{
            opacity: spring(UiStore.showForecast ? 1 : 0),
            x: spring(UiStore.showForecast ? 0 : -100)
          }}
        >
          {styles => (
            <Forecast
              style={{
                opacity: styles.opacity,
                transform: `translateX(${styles.x}%)`
              }}
            >
              {weather.forecast.map(daily => (
                <Daily {...daily} key={daily.date} />
              ))}
            </Forecast>
          )}
        </Motion>

        <Current
          temp={weather.current.temp}
          city={weather.city}
          toggleForecast={() => {
            UiStore.toggleForecast();
          }}
        />
        <Time time={WeatherStore.time} />
        <Today date={weather.current.date} />

        <Chat city={city} />
      </CityContainer>
    );
  }
}
