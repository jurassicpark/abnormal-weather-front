import React, { Component } from "react";
import { BrowserRouter, Switch, Route } from "react-router-dom";
import { Provider } from "mobx-react";

import UiStore from "./stores/UiStore";
import WeatherStore from "./stores/WeatherStore";

import Home from "./pages/Home";
import City from "./pages/City";

class App extends Component {
  render() {
    return (
      <Provider UiStore={UiStore} WeatherStore={WeatherStore}>
        <BrowserRouter>
          <Switch>
            <Route exact path="/" component={Home} />
            <Route exact path="/:city" component={City} />
          </Switch>
        </BrowserRouter>
      </Provider>
    );
  }
}

export default App;
