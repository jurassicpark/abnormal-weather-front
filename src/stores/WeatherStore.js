import { observable, action } from "mobx";
import axios from "axios";

class WeatherStore {
  @observable nearby = [];
  @observable address = "";
  @observable time = new Date().toISOString();
  @observable weather = null;
  @observable loadWeatherError = false;

  @action
  tick = () => {
    this.time = new Date().toISOString();
  };

  getPosition = (options = {}) => {
    return new Promise((resolve, reject) => {
      navigator.geolocation.getCurrentPosition(resolve, reject, options);
    });
  };

  @action
  loadNearby = async () => {
    try {
      this.loadLocalNearby();

      const position = await this.getPosition();
      const { latitude, longitude } = position.coords;
      const response = await axios.get(
        "https://abnormal-weather-api.herokuapp.com/cities/nearby",
        {
          params: { latitude, longitude }
        }
      );
      this.nearby = response.data;
      localStorage.setItem("nearby", JSON.stringify(response.data));
    } catch (err) {
      console.log(err);
    }
  };

  @action
  loadLocalNearby = () => {
    const localNearby = localStorage.getItem("nearby");
    if (localNearby) {
      this.nearby = JSON.parse(localNearby);
    }
  };

  @action
  fetchWeather = async city => {
    this.loadWeatherError = false;
    this.weather = null;

    try {
      const response = await axios.get(
        `https://abnormal-weather-api.herokuapp.com/cities/search`,
        { params: { city } }
      );
      this.weather = response.data;
    } catch (error) {
      this.loadWeatherError = true;
    }
  };
}

export default new WeatherStore();
