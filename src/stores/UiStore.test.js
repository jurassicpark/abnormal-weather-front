import UiStore from "./UiStore";

it("toggles forecast flag", () => {
  UiStore.showForecast = false;

  UiStore.toggleForecast();

  expect(UiStore.showForecast).toBe(true);
});

it("toggles chat flag", () => {
  UiStore.showChat = false;

  UiStore.toggleChat();

  expect(UiStore.showChat).toBe(true);
});
