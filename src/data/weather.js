export default {
  city: "Toronto",

  current: {
    date: new Date().toString(),
    temp: -2,
    weather: "Sunny"
  },

  forecast: [
    {
      date: "2017-01-18",
      high: -5,
      low: -15,
      weather: "Sunny"
    },
    {
      date: "2017-01-19",
      high: -5,
      low: -15,
      weather: "Cloudy"
    },
    {
      date: "2017-01-20",
      high: -5,
      low: -15,
      weather: "Sunny"
    },
    {
      date: "2017-01-21",
      high: -5,
      low: -15,
      weather: "Snowy"
    },
    {
      date: "2017-01-22",
      high: -5,
      low: -15,
      weather: "Snowy"
    },
    {
      date: "2017-01-23",
      high: -5,
      low: -15,
      weather: "Foggy"
    },
    {
      date: "2017-01-24",
      high: -5,
      low: -15,
      weather: "Sunny"
    }
  ]
};
